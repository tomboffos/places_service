package models

import (
	"gorm.io/gorm"
)

type Place struct {
	gorm.Model
	Title   string `json:"title"`
	UserId  uint   `json:"user_id"`
	Address string `json:"address"`
}
