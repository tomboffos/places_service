package places

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/tomboffos/companion/places_service/pkg/common/models"
	"net/http"
)

func (h *handler) GetPlaces(c *gin.Context) {
	value := c.Param("value")

	var places []models.Place

	err := h.DB.Where("title LIKE ? or address LIKE ?", value+"%", value+"%").Find(&places).Error

	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"places": places,
	})
	return
}
