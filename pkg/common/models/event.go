package models

import (
	"gorm.io/gorm"
	"time"
)

type Event struct {
	gorm.Model
	Title       string     `json:"title"`
	Description string     `json:"description"`
	PlaceId     uint       `json:"place_id"`
	EventTime   *time.Time `json:"event_time"`
}
