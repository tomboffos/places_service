package places

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/tomboffos/companion/places_service/pkg/common/models"
	"net/http"
)

type CreatePlaceBody struct {
	Title   string `json:"title"   binding:"required"`
	UserId  uint   `json:"user_id" binding:"required"`
	Address string `json:"address" binding:"required"`
}

func (h *handler) CreatePlace(c *gin.Context) {
	body := CreatePlaceBody{}

	if err := c.BindJSON(&body); err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	var place models.Place

	place.Title = body.Title
	place.Address = body.Address
	place.UserId = body.UserId

	if result := h.DB.Create(&place); result.Error != nil {
		c.AbortWithError(http.StatusInternalServerError, result.Error)
		return
	}

	c.JSON(http.StatusCreated, place)
}
