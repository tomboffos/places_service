package main

import (
	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
	"gitlab.com/tomboffos/companion/places_service/pkg/common/db"
	"gitlab.com/tomboffos/companion/places_service/pkg/places"
	"gorm.io/gorm"
)

func setupRouter(h *gorm.DB) *gin.Engine {
	r := gin.Default()

	places.RegisterRoutes(r, h)

	return r
}

func main() {
	viper.SetConfigFile("./pkg/common/envs/.env")
	viper.ReadInConfig()

	port := viper.Get("PORT").(string)
	dbUrl := viper.Get("DB_URL").(string)

	h := db.Init(dbUrl)
	r := setupRouter(h)

	r.Run(port)
}
